package com.ruoyi.project.jc.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ruoyi.framework.web.domain.AjaxResult;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

/**
 * @author:leichengyang
 * @desc:com.ruoyi.project.jc
 * @date:2020-05-08
 */
@RestController
@RequestMapping("/jc")
public class JcController {

    /**
     * 获取足球十四场开奖结果
     */
    @GetMapping(value = "/getFootBallResult")
    public static AjaxResult getFootBallResult(@RequestBody Map<String, Object> map) {
        RestTemplate restTemplate = new RestTemplate();
        String resultStr = restTemplate.getForObject("http://f.apiplus.net/newly.do?code=zcsfc&format=json", String.class);
        JSONObject jsonObject = JSONObject.parseObject(resultStr);
        Object rows = jsonObject.get("rows");
        if (!StringUtils.isEmpty(rows)) {
            JSONArray dataArr = (JSONArray) jsonObject.get("data");
            return AjaxResult.success(dataArr);
        }
        return AjaxResult.success();
    }

    /**
     * 获取任选9场开奖结果
     */
    @GetMapping(value = "/getAnyChooseNineResult")
    public static AjaxResult getAnyChooseNineResult(@RequestBody Map<String, Object> map) {
        RestTemplate restTemplate = new RestTemplate();
        String resultStr = restTemplate.getForObject("http://f.apiplus.net/newly.do?code=zcsfc&format=json", String.class);
        JSONObject jsonObject = JSONObject.parseObject(resultStr);
        Object rows = jsonObject.get("rows");
        if (!StringUtils.isEmpty(rows)) {
            JSONArray dataArr = (JSONArray) jsonObject.get("data");
            return AjaxResult.success(dataArr);
        }
        return AjaxResult.success();
    }

    /**
     * 获取十一选5开奖结果
     * {msg=操作成功, code=200, data=[
     * {"expect":"2020051222","opencode":"11,05,06,04,08","opentimestamp":1589273165,"opentime":"2020-05-12 16:46:05"}
     * ,{"expect":"2020051221","opencode":"09,08,01,04,07","opentimestamp":1589271921,"opentime":"2020-05-12 16:25:21"}
     * ,{"expect":"2020051220","opencode":"04,09,01,11,07","opentimestamp":1589270642,"opentime":"2020-05-12 16:04:02"}]}
     */
    @GetMapping(value = "/getElevenChooseFiveResult")
    public static AjaxResult getElevenChooseFiveResult(@RequestBody Map<String, Object> map) {
        RestTemplate restTemplate = new RestTemplate();
        String resultStr = restTemplate.getForObject("http://f.apiplus.net/newly.do?code=bj11x5&format=json", String.class);
        JSONObject jsonObject = JSONObject.parseObject(resultStr);
        Object rows = jsonObject.get("rows");
        if (!StringUtils.isEmpty(rows)) {
            JSONArray dataArr = (JSONArray) jsonObject.get("data");
            return AjaxResult.success(dataArr);
        }
        return AjaxResult.success();
    }

    public static void main(String[] args) {
        AjaxResult footBallResult = getElevenChooseFiveResult(null);
        System.out.println(footBallResult);
    }


}
