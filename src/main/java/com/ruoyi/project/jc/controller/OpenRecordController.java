package com.ruoyi.project.jc.controller;


import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.project.jc.domain.OpenRecord;
import com.ruoyi.project.jc.domain.SysUserRecord;
import com.ruoyi.project.jc.service.OpenRecordService;
import com.ruoyi.project.jc.vo.SysUserRecordVo;
import com.ruoyi.project.jc.vo.UserRecordVo;
import org.apache.commons.collections.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author leichengyang
 * @since 2020-05-08
 */
@RestController
@RequestMapping("/open/record")
public class OpenRecordController extends BaseController {

    @Autowired
    private OpenRecordService openRecordService;

    /**
     * 获取开奖记录列表
     * @return
     */
//    @PreAuthorize("@ss.hasPermi('open:record:list')")
    @PostMapping(value = "/list")
    @ResponseBody
    public TableDataInfo getOpenRecordList(SysUserRecordVo userRecord) {
//            int pageNum = MapUtils.getIntValue(map, "pageNum",1);
//            int pageSize = MapUtils.getIntValue(map, "pageSize",10);
            startPage();
            Map<String, Object> map = new HashMap<>();
            putMapValueFromVo(map, userRecord, SysUserRecordVo.class);
//            PageHelper.startPage(pageNum, pageSize);
            List<Map<String,Object>> userRecordList=openRecordService.getOpenRecordList(map);
//            PageInfo<Map<String,Object>> page = new PageInfo<>(userRecordList);

            return getDataTable(userRecordList);
    }

    public void putMapValueFromVo(Map<String, Object> map, Object o, Class<?> c) {
        // 获取类中的全部定义字段
        Field[] fields = c.getDeclaredFields();
        // 循环遍历字段，获取字段相应的属性值
        for (Field field : fields) {
            // 假设不为空。设置可见性，然后返回
            field.setAccessible(true);
            try {
                // 设置字段可见，就可以用get方法获取属性值。
                String fieldName = field.getName();
                Object fieldValue = field.get(o);
                map.put(fieldName, fieldValue);
            } catch (Exception e) {
            }
        }
    }

    @GetMapping("/export")
    public AjaxResult export(SysUserRecord userRecord) {
        Map<String, Object> map = new HashMap<>();
        putMapValueFromVo(map, userRecord, SysUserRecordVo.class);
        List<OpenRecord> list = openRecordService.getOpenRecordListVo(map);
        ExcelUtil<OpenRecord> util = new ExcelUtil<OpenRecord>(OpenRecord.class);
        return util.exportExcel(list, "记录数据");
    }

}
