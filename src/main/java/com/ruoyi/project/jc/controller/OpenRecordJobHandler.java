package com.ruoyi.project.jc.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ruoyi.project.jc.domain.OpenRecord;
import com.ruoyi.project.jc.service.OpenRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * @author:leichengyang
 * @desc:com.ruoyi.project.jc.controller
 * @date:2020-05-12
 */
@Component
public class OpenRecordJobHandler {

    @Autowired
    private OpenRecordService openRecordService;

    /**
     * 彩票类型 1.竞彩足球   2.竞彩篮球    3.足球14场    4.任选9场   5.11选5   6.大乐透   7.七星彩  8.排列3   9.排列5
     */
    private static final String CP_TYPE_FOOTBALL="1";
    private static final String CP_TYPE_BASKETBALL="2";
    private static final String CP_TYPE_FOOTBALL_FORTEEN="3";
    private static final String CP_TYPE_ANY_CHOOSE_NINE="4";
    private static final String CP_TYPE_ELEVEN_CHOOSE_FIVE="5";
    private static final String CP_TYPE_BIG_HAPPY="6";
    private static final String CP_TYPE_SEVEN_STAR="7";
    private static final String CP_TYPE_SORT_THREE="8";
    private static final String CP_TYPE_SORT_FIVE="8";


    @Scheduled(cron = "0 0 1/1 * * ? ")
//    @Scheduled(cron = "0 0/5 * * * ? ")
    public void execute(){
        getOpenRecord();
    }

    private void getOpenRecord() {
        List<OpenRecord> list = new ArrayList<>();

        try {
            JSONArray footBallResult = getFootBallForteenResult(null);
            if (!footBallResult.isEmpty()){
                for (Object jsonObject : footBallResult) {
                    JSONObject jObject= (JSONObject) jsonObject;
                    Object expect = jObject.get("expect");
                    Object opentime = jObject.get("opentime");
                    LocalDateTime parse = LocalDateTime.parse(opentime.toString(), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
                    Map<String, Object> map = new HashMap<>();
                    map.put("expect",expect);
                    map.put("cp_type",CP_TYPE_FOOTBALL_FORTEEN);
                    List<Map<String, Object>> openRecordList = openRecordService.getOpenRecordList(map);
                    if (openRecordList.isEmpty()) {
                        OpenRecord openRecord = new OpenRecord();
                        openRecord.setCpType(CP_TYPE_FOOTBALL_FORTEEN);
                        openRecord.setCreateTime(new Date());
                        openRecord.setExpect(expect.toString());
                        openRecord.setOpencode(jObject.get("opencode").toString());
                        openRecord.setOpentime(parse);
                        list.add(openRecord);
                    }
                }
            }
        } catch (Exception e) {
//            e.printStackTrace();
        }
        try {

            JSONArray anyChooseNine = getAnyChooseNineResult(null);
            if (!anyChooseNine.isEmpty()){
                for (Object jsonObject : anyChooseNine) {
                    JSONObject jObject= (JSONObject) jsonObject;
                    Object expect = jObject.get("expect");
                    Object opentime = jObject.get("opentime");
                    LocalDateTime parse = LocalDateTime.parse(opentime.toString(), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
                    Map<String, Object> map = new HashMap<>();
                    map.put("expect",expect);
                    map.put("cp_type",CP_TYPE_ANY_CHOOSE_NINE);
                    List<Map<String, Object>> openRecordList = openRecordService.getOpenRecordList(map);
                    if (openRecordList.isEmpty()) {
                        OpenRecord openRecord = new OpenRecord();
                        openRecord.setCpType(CP_TYPE_ANY_CHOOSE_NINE);
                        openRecord.setCreateTime(new Date());
                        openRecord.setExpect(expect.toString());
                        openRecord.setOpencode(jObject.get("opencode").toString());
                        openRecord.setOpentime(parse);
                        list.add(openRecord);
                    }
                }
            }
        } catch (Exception e) {
//            e.printStackTrace();
        }
        try {


            JSONArray elevenChooseFive = getElevenChooseFiveResult(null);
            if (!elevenChooseFive.isEmpty()){
                for (Object jsonObject : elevenChooseFive) {
                    JSONObject jObject= (JSONObject) jsonObject;
                    Object expect = jObject.get("expect");
                    Object opentime = jObject.get("opentime");
                    LocalDateTime parse = LocalDateTime.parse(opentime.toString(), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
                    Map<String, Object> map = new HashMap<>();
                    map.put("expect",expect);
                    map.put("cp_type",CP_TYPE_ELEVEN_CHOOSE_FIVE);
                    List<Map<String, Object>> openRecordList = openRecordService.getOpenRecordList(map);
                    if (openRecordList.isEmpty()) {
                        OpenRecord openRecord = new OpenRecord();
                        openRecord.setCpType(CP_TYPE_ELEVEN_CHOOSE_FIVE);
                        openRecord.setCreateTime(new Date());
                        openRecord.setExpect(expect.toString());
                        openRecord.setOpencode(jObject.get("opencode").toString());
                        openRecord.setOpentime(parse);
                        list.add(openRecord);
                    }
                }
            }
        } catch (Exception e) {
//            e.printStackTrace();
        }

        if (!list.isEmpty()){
            openRecordService.saveBatch(list);
        }

    }


    /**
     * 获取足球十四场开奖结果
     */
    public static JSONArray getFootBallForteenResult(Map<String, Object> map) {
        RestTemplate restTemplate = new RestTemplate();
        String resultStr = restTemplate.getForObject("http://f.apiplus.net/newly.do?code=zcsfc&format=json", String.class);
        JSONObject jsonObject = JSONObject.parseObject(resultStr);
        Object rows = jsonObject.get("rows");
        if (!StringUtils.isEmpty(rows)) {
            JSONArray dataArr = (JSONArray) jsonObject.get("data");
            return dataArr;
        }
        return null;
    }

    /**
     * 获取任选9场开奖结果
     */
    public static JSONArray getAnyChooseNineResult(Map<String, Object> map) {
        RestTemplate restTemplate = new RestTemplate();
        String resultStr = restTemplate.getForObject("http://f.apiplus.net/newly.do?code=zcsfc&format=json", String.class);
        JSONObject jsonObject = JSONObject.parseObject(resultStr);
        Object rows = jsonObject.get("rows");
        if (!StringUtils.isEmpty(rows)) {
            JSONArray dataArr = (JSONArray) jsonObject.get("data");
            return dataArr;
        }
        return null;
    }

    /**
     * 获取十一选5开奖结果
     * {msg=操作成功, code=200, data=[
     * {"expect":"2020051222","opencode":"11,05,06,04,08","opentimestamp":1589273165,"opentime":"2020-05-12 16:46:05"}
     * ,{"expect":"2020051221","opencode":"09,08,01,04,07","opentimestamp":1589271921,"opentime":"2020-05-12 16:25:21"}
     * ,{"expect":"2020051220","opencode":"04,09,01,11,07","opentimestamp":1589270642,"opentime":"2020-05-12 16:04:02"}]}
     */
    public static JSONArray getElevenChooseFiveResult(Map<String, Object> map) {
        RestTemplate restTemplate = new RestTemplate();
        String resultStr = restTemplate.getForObject("http://f.apiplus.net/newly.do?code=bj11x5&format=json", String.class);
        JSONObject jsonObject = JSONObject.parseObject(resultStr);
        Object rows = jsonObject.get("rows");
        if (!StringUtils.isEmpty(rows)) {
            JSONArray dataArr = (JSONArray) jsonObject.get("data");
            return dataArr;
        }
        return null;
    }

}
