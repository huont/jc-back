package com.ruoyi.project.jc.controller;


import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.project.jc.domain.SysUserRecord;
import com.ruoyi.project.jc.service.SysUserRecordService;
import com.ruoyi.project.jc.vo.SysUserRecordVo;
import com.ruoyi.project.jc.vo.UserRecordVo;
import com.ruoyi.project.system.domain.SysUser;
import com.ruoyi.project.system.service.ISysUserService;
import org.apache.commons.collections.MapUtils;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.*;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author leichengyang
 * @since 2020-05-08
 */
@RestController
@RequestMapping("/user/record")
public class SysUserRecordController extends BaseController {

    @Autowired
    private SysUserRecordService sysUserRecordService;

    @Autowired
    private ISysUserService sysUserService;

    /**
     * 获取用户购彩记录列表
     *
     * @return
     */
//    @PreAuthorize("@ss.hasPermi('user:record:list')")
    @PostMapping(value = "/list")
    @ResponseBody
    public TableDataInfo getUserRecordList(SysUserRecordVo userRecord) {
//        try {
//        int pageNum = MapUtils.getIntValue(map, "pageNum", 1);
//        int pageSize = MapUtils.getIntValue(map, "pageSize", 10);
        startPage();
        Map<String, Object> map = new HashMap<>();
        String cpNo = userRecord.getCpNo();
        String cpType = userRecord.getCpType();
        String expect = userRecord.getExpect();
        String nickName = userRecord.getNickName();
        String userId = userRecord.getUserId();
        putMapValueFromVo(map, userRecord, SysUserRecordVo.class);

//            PageHelper.startPage(pageNum, pageSize);
        List<Map<String, Object>> userRecordList = sysUserRecordService.getUserRecordList(map);
        PageInfo<Map<String, Object>> page = new PageInfo<>(userRecordList);

        return getDataTable(userRecordList);
//        } catch (Exception e) {
//            e.printStackTrace();
//            return AjaxResult.error();
//        }
    }


    /**
     * @param o 操作对象
     * @param c 操作类。用于获取类中的方法
     * @return
     * @MethodName : getString
     * @Description : 获取类中全部属性及属性值
     */
    public void putMapValueFromVo(Map<String, Object> map, Object o, Class<?> c) {
        // 获取类中的全部定义字段
        Field[] fields = c.getDeclaredFields();
        // 循环遍历字段，获取字段相应的属性值
        for (Field field : fields) {
            // 假设不为空。设置可见性，然后返回
            field.setAccessible(true);
            try {
                // 设置字段可见，就可以用get方法获取属性值。
                String fieldName = field.getName();
                Object fieldValue = field.get(o);
                map.put(fieldName, fieldValue);
            } catch (Exception e) {
            }
        }
    }

//    @PreAuthorize("@ss.hasPermi('user:record:query')")
    @GetMapping(value = {"/", "/{recordId}"})
    public AjaxResult getInfo(@PathVariable(value = "recordId", required = false) String recordId) {
        AjaxResult ajax = AjaxResult.success();
        if (StringUtils.isNotNull(recordId)) {
            Map<String, Object> map = new HashMap<>();
            map.put("recorId", recordId);
            ajax.put(AjaxResult.DATA_TAG, sysUserRecordService.getUserRecordList(map));
        }
        return ajax;
    }


    /**
     * 新增用户购彩记录列表
     *
     * @return
     */
//    @PreAuthorize("@ss.hasPermi('user:record:add')")
//    @PostMapping(value = "/add")
//    @ResponseBody
//    public AjaxResult saveUserRecord(@RequestBody SysUserRecord sysUserRecord) {
//
//        if (sysUserRecord.getUserId() != null) {
//            SysUser sysUser = sysUserService.selectUserById(sysUserRecord.getUserId());
//            if (sysUser != null) {
//                BigDecimal totalPrice = sysUserRecord.getTotalPrice();
//                BigDecimal userAmount = sysUser.getUserAmount();
//                BigDecimal subtract = userAmount.subtract(totalPrice);
//                sysUser.setUserAmount(subtract);
//                sysUserService.updateUser(sysUser);
//                return AjaxResult.success(sysUser);
//            }
//        } else {
//            sysUserRecordService.saveUserRecord(sysUserRecord);
//        }
//        return AjaxResult.success(sysUserRecord);
//    }
//    @PreAuthorize("@ss.hasPermi('user:record:add')")
    @PostMapping(value = "/add")
    @ResponseBody
    public AjaxResult saveUserRecord(@RequestBody List<SysUserRecord> recordList) {

        List<SysUserRecord> list = new ArrayList<>();
        if (!recordList.isEmpty()) {
            for (SysUserRecord sysUserRecord : recordList) {
                SysUser sysUser = sysUserService.selectUserById(sysUserRecord.getUserId());
                if (sysUser != null) {
                    BigDecimal totalPrice = sysUserRecord.getTotalPrice();
                    BigDecimal userAmount = sysUser.getUserAmount();
                    BigDecimal subtract = userAmount.subtract(totalPrice);
                    sysUser.setUserAmount(subtract);
                    sysUserService.updateUser(sysUser);
                }
                sysUserRecord.setRecordId(UUID.randomUUID().toString().replace("-",""));
            }
                sysUserRecordService.saveUserRecordBatch(recordList);
                list.addAll(recordList);
        }
        return AjaxResult.success(list);
    }

    /**
     * 修改用户购彩记录列表
     *
     * @return
     */
//    @PreAuthorize("@ss.hasPermi('user:record:edit')")
    @PutMapping(value = "/edit")
    @ResponseBody
    public AjaxResult updateUserRecord(@RequestBody SysUserRecord sysUserRecord) {

        if (sysUserRecord.getRecordId() != null) {
            sysUserRecordService.updateUserRecord(sysUserRecord);
        }
        return AjaxResult.success();
    }

    /**
     * 删除用户购彩记录列表
     *
     * @return
     */
//    @PreAuthorize("@ss.hasPermi('user:record:remove')")
    @DeleteMapping(value = "/{recordIds}")
    @ResponseBody
    public AjaxResult delUserRecord(@PathVariable String[] recordIds) {

        sysUserRecordService.delUserRecordByIds(recordIds);
        return AjaxResult.success();
    }


//    @PreAuthorize("@ss.hasPermi('user:record:export')")
    @GetMapping("/export")
    public AjaxResult export(SysUserRecord userRecord) {
        Map<String, Object> map = new HashMap<>();
        putMapValueFromVo(map, userRecord, SysUserRecordVo.class);
        List<UserRecordVo> list = sysUserRecordService.getUserRecordListVo(map);
        ExcelUtil<UserRecordVo> util = new ExcelUtil<UserRecordVo>(UserRecordVo.class);
        return util.exportExcel(list, "记录数据");
    }

}
