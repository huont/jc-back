package com.ruoyi.project.jc.domain;

import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author leichengyang
 * @since 2020-05-08
 */
public class OpenRecord extends BaseEntity {

    private static final long serialVersionUID = 1L;

    private Long openId;

    /**
     * 开奖号码
     */
    @Excel(name = "开奖号码")
    private String opencode;

    /**
     * 期数编号
     */
    @Excel(name = "期数编号")
    private String expect;

    /**
     * 开奖时间
     */
    @Excel(name = "开奖时间")
    private LocalDateTime opentime;

    /**
     * 彩票类型
     */
    private String cpType;

    /**
     * 创建时间
     */
    @Excel(name = "创建时间")
    private Date createTime;

    @Excel(name = "彩票名称")
    private String cpTypeNm;

    public String getCpTypeNm() {
        return cpTypeNm;
    }

    public void setCpTypeNm(String cpTypeNm) {
        this.cpTypeNm = cpTypeNm;
    }

    public Long getOpenId() {
        return openId;
    }

    public void setOpenId(Long openId) {
        this.openId = openId;
    }

    public String getOpencode() {
        return opencode;
    }

    public void setOpencode(String opencode) {
        this.opencode = opencode;
    }

    public String getExpect() {
        return expect;
    }

    public void setExpect(String expect) {
        this.expect = expect;
    }

    public LocalDateTime getOpentime() {
        return opentime;
    }

    public void setOpentime(LocalDateTime opentime) {
        this.opentime = opentime;
    }

    public String getCpType() {
        return cpType;
    }

    public void setCpType(String cpType) {
        this.cpType = cpType;
    }

    @Override
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
