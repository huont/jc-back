package com.ruoyi.project.jc.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.framework.web.domain.BaseEntity;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author leichengyang
 * @since 2020-05-08
 */
public class SysUserRecord extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 记录id
     */
    private String recordId;

    /**
     * 彩票编号
     */
    private String cpNo;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 彩票类型 1.足球十四场 2.任选9场  3.十一选5
     */
    private String cpType;

    /**
     * 彩票名称
     */
    private String cpName;

    /**
     * 购买数量
     */
    private Integer buyNum;

    /**
     * 购买单价
     */
    private BigDecimal buyPrice;

    /**
     * 购买倍数
     */
    private Integer buyMultiple;

    /**
     * 购买合计总价
     */
    private BigDecimal totalPrice;

    /**
     * 购买时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date buyDate;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date createTime;

    /**
     * 期数编号
     */
    private String expect;




    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public String getCpNo() {
        return cpNo;
    }

    public void setCpNo(String cpNo) {
        this.cpNo = cpNo;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getCpType() {
        return cpType;
    }

    public void setCpType(String cpType) {
        this.cpType = cpType;
    }

    public String getCpName() {
        return cpName;
    }

    public void setCpName(String cpName) {
        this.cpName = cpName;
    }

    public Integer getBuyNum() {
        return buyNum;
    }

    public void setBuyNum(Integer buyNum) {
        this.buyNum = buyNum;
    }

    public BigDecimal getBuyPrice() {
        return buyPrice;
    }

    public void setBuyPrice(BigDecimal buyPrice) {
        this.buyPrice = buyPrice;
    }

    public Integer getBuyMultiple() {
        return buyMultiple;
    }

    public void setBuyMultiple(Integer buyMultiple) {
        this.buyMultiple = buyMultiple;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Date getBuyDate() {
        return buyDate;
    }

    public void setBuyDate(Date buyDate) {
        this.buyDate = buyDate;
    }

    @Override
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getExpect() {
        return expect;
    }

    public void setExpect(String expect) {
        this.expect = expect;
    }
}
