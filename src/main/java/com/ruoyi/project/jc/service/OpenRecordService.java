package com.ruoyi.project.jc.service;

import com.ruoyi.project.jc.domain.OpenRecord;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author leichengyang
 * @since 2020-05-08
 */
public interface OpenRecordService {

    List<Map<String, Object>> getOpenRecordList(Map<String, Object> map);

    void saveBatch(List<OpenRecord> list);

    List<OpenRecord> getOpenRecordListVo(Map<String, Object> map);
}
