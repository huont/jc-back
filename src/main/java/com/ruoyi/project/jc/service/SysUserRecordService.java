package com.ruoyi.project.jc.service;

import com.ruoyi.project.jc.domain.SysUserRecord;
import com.ruoyi.project.jc.vo.UserRecordVo;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author leichengyang
 * @since 2020-05-08
 */
public interface SysUserRecordService {

    List<Map<String, Object>> getUserRecordList(Map<String, Object> map);

    void saveUserRecord(SysUserRecord sysUserRecord);

    void updateUserRecord(SysUserRecord sysUserRecord);

    void delUserRecord(SysUserRecord sysUserRecord);

    int delUserRecordByIds(String[] recordIds);

    List<UserRecordVo> getUserRecordListVo(Map<String, Object> map);

    void saveUserRecordBatch(List<SysUserRecord> recordList);
}
