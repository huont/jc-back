package com.ruoyi.project.jc.service.impl;

import com.ruoyi.project.jc.domain.OpenRecord;
import com.ruoyi.project.jc.mapper.OpenRecordMapper;
import com.ruoyi.project.jc.service.OpenRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author leichengyang
 * @since 2020-05-08
 */
@Service
public class OpenRecordServiceImpl implements OpenRecordService {

    @Autowired
    private OpenRecordMapper openRecordMapper;

    @Override
    public List<Map<String, Object>> getOpenRecordList(Map<String, Object> map) {

        return openRecordMapper.getOpenRecordList(map);
    }

    @Override
    public void saveBatch(List<OpenRecord> list) {
        openRecordMapper.saveBatch(list);
    }

    @Override
    public List<OpenRecord> getOpenRecordListVo(Map<String, Object> map) {
        return openRecordMapper.getOpenRecordListVo(map);
    }
}
