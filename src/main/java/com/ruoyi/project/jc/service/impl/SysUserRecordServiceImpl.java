package com.ruoyi.project.jc.service.impl;

import com.ruoyi.common.exception.CustomException;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.project.jc.domain.SysUserRecord;
import com.ruoyi.project.jc.mapper.SysUserRecordMapper;
import com.ruoyi.project.jc.service.SysUserRecordService;
import com.ruoyi.project.jc.vo.UserRecordVo;
import com.ruoyi.project.system.domain.SysUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author leichengyang
 * @since 2020-05-08
 */
@Service
public class SysUserRecordServiceImpl implements SysUserRecordService {

    @Autowired
    private SysUserRecordMapper sysUserRecordMapper;

    @Override
    public List<Map<String, Object>> getUserRecordList(Map<String, Object> map) {

        return sysUserRecordMapper.getUserRecordList(map);
    }

    @Override
    public void saveUserRecord(SysUserRecord sysUserRecord) {
        sysUserRecordMapper.saveUserRecord(sysUserRecord);
    }

    @Override
    public void updateUserRecord(SysUserRecord sysUserRecord) {
        sysUserRecordMapper.updateUserRecord(sysUserRecord);
    }

    @Override
    public void delUserRecord(SysUserRecord sysUserRecord) {
        sysUserRecordMapper.delUserRecord(sysUserRecord);
    }

    @Override
    public int delUserRecordByIds(String[] recordIds) {
        return sysUserRecordMapper.delUserRecordByIds(recordIds);
    }

    @Override
    public List<UserRecordVo> getUserRecordListVo(Map<String, Object> map) {
        return sysUserRecordMapper.getUserRecordListVo(map);
    }

    @Override
    public void saveUserRecordBatch(List<SysUserRecord> recordList) {
        sysUserRecordMapper.saveUserRecordBatch(recordList);
    }

}
