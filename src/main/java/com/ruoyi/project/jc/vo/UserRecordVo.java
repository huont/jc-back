package com.ruoyi.project.jc.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author leichengyang
 * @since 2020-05-08
 */
public class UserRecordVo {

    private static final long serialVersionUID = 1L;

    /**
     * 记录id
     */
    private Long recordId;

    /**
     * 彩票编号
     */
    @Excel(name = "彩票编号")
    private String cpNo;

    @Excel(name = "开奖号码")
    private String opencode;

    @Excel(name = "手机号码")
    private String phonenumber;

    @Excel(name = "是否中奖")
    private String isWin;

    @Excel(name = "用户名称")
    private String nickName;

    @Excel(name = "彩票名称")
    private String cpTypeNm;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 彩票类型 1.足球十四场 2.任选9场  3.十一选5
     */
    private String cpType;

    /**
     * 彩票名称
     */
    private String cpName;

    /**
     * 购买数量
     */
    @Excel(name = "数量")
    private Integer buyNum;

    /**
     * 购买单价
     */
    @Excel(name = "单价")
    private BigDecimal buyPrice;

    /**
     * 购买倍数
     */
    @Excel(name = "倍数")
    private Integer buyMultiple;

    /**
     * 购买合计总价
     */
    @Excel(name = "总价")
    private BigDecimal totalPrice;

    /**
     * 购买时间
     */
    @Excel(name = "购买时间")
    @JsonFormat(pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date buyDate;

    /**
     * 创建时间
     */
    @Excel(name = "创建时间")
    private Date createTime;

    /**
     * 期数编号
     */
    @Excel(name = "期数编号")
    private String expect;


    public String getOpencode() {
        return opencode;
    }

    public void setOpencode(String opencode) {
        this.opencode = opencode;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public String getIsWin() {
        return isWin;
    }

    public void setIsWin(String isWin) {
        this.isWin = isWin;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getCpTypeNm() {
        return cpTypeNm;
    }

    public void setCpTypeNm(String cpTypeNm) {
        this.cpTypeNm = cpTypeNm;
    }

    public Long getRecordId() {
        return recordId;
    }

    public void setRecordId(Long recordId) {
        this.recordId = recordId;
    }

    public String getCpNo() {
        return cpNo;
    }

    public void setCpNo(String cpNo) {
        this.cpNo = cpNo;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getCpType() {
        return cpType;
    }

    public void setCpType(String cpType) {
        this.cpType = cpType;
    }

    public String getCpName() {
        return cpName;
    }

    public void setCpName(String cpName) {
        this.cpName = cpName;
    }

    public Integer getBuyNum() {
        return buyNum;
    }

    public void setBuyNum(Integer buyNum) {
        this.buyNum = buyNum;
    }

    public BigDecimal getBuyPrice() {
        return buyPrice;
    }

    public void setBuyPrice(BigDecimal buyPrice) {
        this.buyPrice = buyPrice;
    }

    public Integer getBuyMultiple() {
        return buyMultiple;
    }

    public void setBuyMultiple(Integer buyMultiple) {
        this.buyMultiple = buyMultiple;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Date getBuyDate() {
        return buyDate;
    }

    public void setBuyDate(Date buyDate) {
        this.buyDate = buyDate;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getExpect() {
        return expect;
    }

    public void setExpect(String expect) {
        this.expect = expect;
    }
}
